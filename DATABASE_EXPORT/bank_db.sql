-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2015 at 11:23 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bank_db`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `deductMoney`(OUT _balance int(4), IN amount int(4), IN _account char(10))
BEGIN
	SELECT balance-amount INTO _balance FROM account WHERE account_number=_account;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllCreditTransactionMakeOnDate`(IN order_day date)
BEGIN
	SELECT * FROM credit_transaction WHERE ordering_date=order_day;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCreTrans`()
BEGIN
	DELETE FROM credit_transaction WHERE account_number="9006070182";
	 
	START TRANSACTION;
		insert into credit_transaction  values (NULL,"9006070182","DKIT","refund",CURRENT_TIMESTAMP,300);
		UPDATE account set balance=balance+300 where account_number="9006070182";
		SET @accountType= (SELECT account_type from account where account_number="9006070182");
		IF (@accountType="SAVING") THEN
			ROLLBACK;
		ELSE 
			COMMIT;
		END IF;
		 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getcustomer`()
BEGIN
	SELECT * FROM customer;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getDebitTransactionByMonth`(IN trans_month int(4))
BEGIN
	SELECT * FROM debit_transaction_record WHERE MONTH(transaction_date)=trans_month;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getMaxAmountDebit`(OUT _max int(4))
BEGIN
	SELECT count(*) INTO _max FROM customer;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getTotalNumberCustomer`(OUT total int(4))
BEGIN
	SELECT count(*) INTO total FROM customer;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getTrans`()
BEGIN
	DELETE FROM debit_transaction_record WHERE account_number="9006070182";
	 
	START TRANSACTION;
		insert into debit_transaction_record(transaction_id,account_number,transaction_date,description,amount) values ("TES001","9006070182",CURRENT_TIMESTAMP,"POS_PAY_TESCO_STORE",30);
		UPDATE account set balance=balance-100 where account_number="9006070182";
		SET @balance= (SELECT balance from account where account_number="9006070182");
		IF (@balance>0) THEN
			COMMIT;
		ELSE 
			ROLLBACK;
		END IF;
		 
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `account_number` char(10) NOT NULL,
  `customer_id` varchar(10) NOT NULL,
  `account_type` varchar(10) DEFAULT NULL,
  `balance` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`account_number`, `customer_id`, `account_type`, `balance`) VALUES
('9006070182', 'AA012', 'Normal', '3996'),
('9007909905', 'AA013', 'Student', '2369'),
('9026940758', 'AA024', 'Normal', '3467'),
('9049563021', 'AA006', 'Saving', '897'),
('9053001786', 'AA015', 'Student', '6874'),
('9053711554', 'AA017', 'Normal', '2002'),
('9057632449', 'AA024', 'Normal', '3996'),
('9106842725', 'AA002', 'Student', '1537'),
('9117886764', 'AA013', 'Normal', '5882'),
('9122756836', 'AA013', 'Normal', '581'),
('9139429747', 'AA021', 'Normal', '3791'),
('9160935361', 'AA019', 'Normal', '2251'),
('9170466379', 'AA008', 'Normal', '3315'),
('9171333860', 'AA005', 'Student', '1094'),
('9180714679', 'AA005', 'Student', '7431'),
('9190690539', 'AA013', 'Normal', '463'),
('9199612897', 'AA007', 'Saving', '282'),
('9240783432', 'AA007', 'Normal', '2946'),
('9246078021', 'AA011', 'Saving', '5075'),
('9250627218', 'AA016', 'Student', '388'),
('9271385350', 'AA006', 'Normal', '3896'),
('9277905086', 'AA003', 'Saving', '6304'),
('9282560811', 'AA020', 'Normal', '4117'),
('9290598108', 'AA009', 'Normal', '4944'),
('9302688509', 'AA016', 'Normal', '543'),
('9313472676', 'AA009', 'Normal', '289'),
('9321655045', 'AA002', 'Saving', '1372'),
('9329148856', 'AA011', 'Saving', '2781'),
('9339743150', 'AA015', 'Student', '5000'),
('9387441297', 'AA013', 'Student', '2568'),
('9392591958', 'AA014', 'Normal', '3835'),
('9395870409', 'AA008', 'Normal', '6059'),
('9408848215', 'AA022', 'Normal', '7292'),
('9413290498', 'AA007', 'Normal', '7975'),
('9423446224', 'AA024', 'Normal', '1515'),
('9472535994', 'AA007', 'Saving', '5961'),
('9472647151', 'AA009', 'Normal', '334'),
('9474630153', 'AA008', 'Normal', '6277'),
('9489506437', 'AA008', 'Normal', '6837'),
('9519400453', 'AA010', 'Student', '1414'),
('9525211557', 'AA001', 'Normal', '1151'),
('9532131326', 'AA009', 'Student', '790'),
('9555465438', 'AA005', 'Student', '2130'),
('9556098736', 'AA007', 'Student', '714'),
('9574137535', 'AA001', 'Normal', '2051'),
('9588568394', 'AA006', 'Student', '6008'),
('9606679404', 'AA010', 'Normal', '6808'),
('9613581656', 'AA014', 'Student', '4000'),
('9636324714', 'AA013', 'Normal', '1436'),
('9637732169', 'AA009', 'Normal', '208'),
('9637752853', 'AA018', 'Normal', '7543'),
('9641840026', 'AA010', 'Normal', '2582'),
('9679486991', 'AA012', 'Saving', '197'),
('9686495998', 'AA009', 'Normal', '5643'),
('9703887466', 'AA024', 'Normal', '2474'),
('9713783740', 'AA023', 'Normal', '6274'),
('9738931489', 'AA013', 'Normal', '6106'),
('9739876701', 'AA009', 'Normal', '2011'),
('9741112071', 'AA016', 'Student', '7449'),
('9766513869', 'AA005', 'Student', '3471'),
('9769749259', 'AA024', 'Normal', '6325'),
('9778368402', 'AA013', 'Student', '5255'),
('9803153746', 'AA004', 'Normal', '6614'),
('9817251257', 'AA024', 'Normal', '5171'),
('9853428626', 'AA006', 'Normal', '3509'),
('9857129843', 'AA003', 'Normal', '3206'),
('9910267651', 'AA012', 'Saving', '2247'),
('9917507359', 'AA001', 'Student', '4207'),
('9920568533', 'AA013', 'Normal', '5347'),
('9933249109', 'AA009', 'Student', '1881'),
('9960029161', 'AA004', 'Normal', '6841'),
('9971543947', 'AA008', 'Student', '1325');

--
-- Triggers `account`
--
DELIMITER //
CREATE TRIGGER `insert_message_on_delete_account` BEFORE DELETE ON `account`
 FOR EACH ROW BEGIN
	INSERT INTO transaction_log 
		VALUES (NULL, CONCAT(OLD.account_number,': is deleted'), CURRENT_TIMESTAMP);
	END
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `insert_message_on_update_balance` AFTER UPDATE ON `account`
 FOR EACH ROW BEGIN
	INSERT INTO transaction_log 
		VALUES (NULL, CONCAT(OLD.balance,': is changed to',NEW.balance), CURRENT_TIMESTAMP);
	END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `block_transaction`
--

CREATE TABLE IF NOT EXISTS `block_transaction` (
  `transaction_id` char(6) NOT NULL,
  `account_number` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `block_transaction`
--

INSERT INTO `block_transaction` (`transaction_id`, `account_number`) VALUES
('MAX001', '9006070182'),
('TES002', '9250627218'),
('ANP001', '9574137535'),
('EIR001', '9574137535'),
('ANP001', '9741112071');

-- --------------------------------------------------------

--
-- Table structure for table `credit_transaction`
--

CREATE TABLE IF NOT EXISTS `credit_transaction` (
`transaction_id` int(11) NOT NULL,
  `account_number` char(10) DEFAULT NULL,
  `ordering_customer` varchar(20) DEFAULT NULL,
  `reference` varchar(20) DEFAULT NULL,
  `ordering_date` date DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `credit_transaction`
--

INSERT INTO `credit_transaction` (`transaction_id`, `account_number`, `ordering_customer`, `reference`, `ordering_date`, `amount`) VALUES
(1, '9713783740', 'Ligula Consectetuer ', 'Wage for January', '2016-01-25', '940.97'),
(2, '9817251257', 'Dapibus PC', 'Refund ', '2015-06-03', '945.07'),
(3, '9778368402', 'Curabitur Massa Vest', 'Travel Fee', '2016-01-21', '817.57'),
(4, '9472647151', 'Aliquam Adipiscing I', 'Graduate Scholarship', '2016-10-17', '792.48'),
(5, '9910267651', 'Sociis Natoque Penat', 'Salary ', '2016-06-12', '649.54'),
(6, '9636324714', 'Est Arcu Ac Institut', 'Graduate Scholarship', '2015-04-05', '935.26'),
(7, '9917507359', 'Sed Corp.', 'Salary ', '2016-05-15', '391.56'),
(8, '9933249109', 'Tincidunt Congue Com', 'Salary ', '2016-07-11', '787.03'),
(9, '9606679404', 'At Iaculis Quis PC', 'Travel Fee', '2016-09-22', '828.53'),
(10, '9329148856', 'Phasellus Libero Inc', 'Salary ', '2015-08-19', '185.44'),
(11, '9769749259', 'Arcu Vestibulum Ut I', 'Graduate Scholarship', '2015-05-29', '330.66'),
(12, '9395870409', 'Ipsum Porta Ltd', 'Salary ', '2015-03-09', '414.94'),
(13, '9313472676', 'Odio Etiam Ligula LL', 'Salary ', '2015-05-06', '889.63'),
(14, '9686495998', 'Sagittis Corporation', 'Salary ', '2016-03-09', '819.65'),
(15, '9519400453', 'Fermentum Arcu Vesti', 'Salary ', '2015-09-17', '243.00'),
(16, '9006070182', 'DKIT', 'refund', '2015-12-10', '300.00');

--
-- Triggers `credit_transaction`
--
DELIMITER //
CREATE TRIGGER `update_balance_on_credit_payment` AFTER INSERT ON `credit_transaction`
 FOR EACH ROW BEGIN
			UPDATE account set balance=balance+NEW.amount where account_number=NEW.account_number;
	END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` varchar(10) NOT NULL,
  `customer_name` varchar(20) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `phone_number` varchar(12) DEFAULT NULL,
  `address` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_name`, `date_of_birth`, `gender`, `phone_number`, `address`) VALUES
('AA001', 'Brenda Young', '1935-12-15', 'F', '863609122', '29 Princes Cres\r'),
('AA002', 'Shirley Bell', '1936-06-21', 'F', '839498093', '1 Cremore Ave \r'),
('AA003', 'Marilyn Howard', '1939-05-26', 'F', '896798938', '52 Henderson Dr\r'),
('AA004', 'Jennifer Sanders', '1939-10-13', 'F', '872823567', '2 Thorngrove Mews\r'),
('AA005', 'Daniel Patterson', '1942-06-08', 'M', '802217734', '113 Manley Rd\r'),
('AA006', 'Sandra Anderson', '1948-01-24', 'F', '862743365', '122 The Broadway\r'),
('AA007', 'Ryan Cox', '1949-11-10', 'M', '834927018', '37 Rickman Dr\r'),
('AA008', 'Johnny Lee', '1952-07-29', 'M', '844809006', '2-2 Greenlaw Rd\r'),
('AA009', 'Jacqueline Gray', '1956-06-25', 'F', '837138971', '141 Okus Rd\r'),
('AA010', 'Aaron Hill', '1962-12-20', 'M', '842121942', '59 Boscobel Dr\r'),
('AA011', 'Michelle Robinson', '1964-06-06', 'F', '861295455', '17 Overdale Gardens\r'),
('AA012', 'Benjamin Watson', '1968-08-13', 'M', '839556158', '96 New Rd\r'),
('AA013', 'Kathryn Thomas', '1973-11-13', 'F', '883475928', '47 Romsey Rd\r'),
('AA014', 'Ruby Williams', '1974-07-13', 'F', '802702372', '6 Mount Bernard Dr\r'),
('AA015', ' Joan Diaz', '1974-07-28', 'F', '881357801', '12 Hubbard Dr\r'),
('AA016', 'Robert Brown', '1976-10-06', 'M', '843433719', '14 Orchard Rd\r'),
('AA017', 'Louise Hall', '1977-11-01', 'M', '879313956', '17 Appletree Rd\r'),
('AA018', 'Linda Coleman', '1978-05-06', 'F', '862358556', '2 Carlibar Gardens\r'),
('AA019', 'Betty Ramirez', '1980-02-18', 'F', '849284779', '93 Marlborough Ave\r'),
('AA020', 'Joyce Bryant', '1981-05-03', 'F', '807327836', '85 Hendal Ln\r'),
('AA021', 'Judith Butler', '1981-09-03', 'F', '836023134', '16 Hird Rd\r'),
('AA022', 'Susan Perry', '1989-01-04', 'F', '818310797', '12Polhill Ave\r'),
('AA023', 'Raymond Lewis', '1989-05-20', 'M', '806127401', '1 Virginia St\r'),
('AA024', 'Wayne Cook', '1991-05-01', 'M', '801510214', '4 Farm Cottages\r');

-- --------------------------------------------------------

--
-- Table structure for table `debit_transaction`
--

CREATE TABLE IF NOT EXISTS `debit_transaction` (
  `transaction_id` char(6) NOT NULL,
  `ledger_id` int(11) NOT NULL,
  `transaction_category` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `debit_transaction`
--

INSERT INTO `debit_transaction` (`transaction_id`, `ledger_id`, `transaction_category`) VALUES
('AER001', 3, 'Booking fee\r'),
('AER002', 3, 'Overweight Baggage\r'),
('ALD001', 7, 'Groceries\r'),
('AMA001', 14, 'Online Payment\r'),
('ANP001', 15, 'Registered Post\r'),
('ARG001', 19, 'Payment In store\r'),
('DKI001', 1, 'Academic fee\r'),
('EIR001', 12, 'Billpay Mobile\r'),
('MAX001', 16, 'Fuel\r'),
('MAX002', 10, 'Broadband Monthly\r'),
('SKY001', 11, 'Monthly plan TV box\r'),
('TES001', 21, 'Groceries\r'),
('TES002', 21, 'Fuel\r'),
('TES003', 21, 'Mobile Bill Pay\r'),
('THR001', 10, 'Mobile Prepay\r');

-- --------------------------------------------------------

--
-- Table structure for table `debit_transaction_record`
--

CREATE TABLE IF NOT EXISTS `debit_transaction_record` (
  `transaction_id` char(6) NOT NULL,
  `account_number` char(10) NOT NULL,
  `transaction_date` date DEFAULT NULL,
  `description` varchar(20) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `debit_transaction_record`
--

INSERT INTO `debit_transaction_record` (`transaction_id`, `account_number`, `transaction_date`, `description`, `amount`) VALUES
('DKI001', '9250627218', '2015-10-27', 'TTR_ ACADEMICFEE_ IN', '4000.00'),
('MAX002', '9392591958', '2015-11-01', 'POS_PAY_DBRDSTATION', '15.75'),
('TES001', '9006070182', '2015-12-10', 'POS_PAY_TESCO_STORE', '30.00'),
('TES001', '9525211557', '2015-10-25', 'POS_PAY_TESCO_STORE_', '7.00');

-- --------------------------------------------------------

--
-- Table structure for table `sale_ledger`
--

CREATE TABLE IF NOT EXISTS `sale_ledger` (
  `ledger_id` int(11) NOT NULL,
  `ledger_name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale_ledger`
--

INSERT INTO `sale_ledger` (`ledger_id`, `ledger_name`) VALUES
(1, 'Dkit '),
(2, 'Job Pay '),
(3, ' Aer Lingus'),
(4, 'Electric Ireland '),
(5, 'Bord Gais Energy'),
(6, 'Irish Water'),
(7, 'Aldi'),
(8, 'Energia'),
(9, 'Lidl'),
(10, 'Three'),
(11, 'Sky Ireland'),
(12, 'Eircom'),
(13, 'Ebay'),
(14, 'Amazon'),
(15, 'An Post '),
(16, 'Maxol'),
(17, 'AIB'),
(18, 'Vodafone'),
(19, 'Argos '),
(20, 'Smiths '),
(21, 'Tesco Ireland');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_log`
--

CREATE TABLE IF NOT EXISTS `transaction_log` (
`message_id` int(4) NOT NULL,
  `message` varchar(255) NOT NULL,
  `message_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `transaction_log`
--

INSERT INTO `transaction_log` (`message_id`, `message`, `message_time`) VALUES
(1, '3596: is changed to3496', '2015-12-10 22:21:32'),
(2, '3496: is changed to3396', '2015-12-10 22:21:33'),
(3, '3396: is changed to3696', '2015-12-10 22:21:33'),
(4, '3696: is changed to3996', '2015-12-10 22:21:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
 ADD PRIMARY KEY (`account_number`), ADD UNIQUE KEY `account_number` (`account_number`), ADD KEY `customer_id` (`customer_id`), ADD KEY `AccountIndex` (`account_number`), ADD KEY `AccountIndexBalance` (`balance`);

--
-- Indexes for table `block_transaction`
--
ALTER TABLE `block_transaction`
 ADD PRIMARY KEY (`transaction_id`,`account_number`), ADD KEY `account_number` (`account_number`);

--
-- Indexes for table `credit_transaction`
--
ALTER TABLE `credit_transaction`
 ADD PRIMARY KEY (`transaction_id`), ADD KEY `account_number` (`account_number`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `debit_transaction`
--
ALTER TABLE `debit_transaction`
 ADD PRIMARY KEY (`transaction_id`), ADD KEY `ledger_id` (`ledger_id`), ADD KEY `TransactionTypeIndex` (`transaction_id`,`ledger_id`);

--
-- Indexes for table `debit_transaction_record`
--
ALTER TABLE `debit_transaction_record`
 ADD PRIMARY KEY (`transaction_id`,`account_number`), ADD KEY `TransactionAccountIndex` (`account_number`), ADD KEY `TransactionIndex` (`transaction_id`,`account_number`), ADD KEY `TransactionIndexOnDate` (`transaction_date`);

--
-- Indexes for table `sale_ledger`
--
ALTER TABLE `sale_ledger`
 ADD PRIMARY KEY (`ledger_id`);

--
-- Indexes for table `transaction_log`
--
ALTER TABLE `transaction_log`
 ADD PRIMARY KEY (`message_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `credit_transaction`
--
ALTER TABLE `credit_transaction`
MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `transaction_log`
--
ALTER TABLE `transaction_log`
MODIFY `message_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
ADD CONSTRAINT `account_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`);

--
-- Constraints for table `block_transaction`
--
ALTER TABLE `block_transaction`
ADD CONSTRAINT `block_transaction_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `debit_transaction` (`transaction_id`),
ADD CONSTRAINT `block_transaction_ibfk_2` FOREIGN KEY (`account_number`) REFERENCES `account` (`account_number`);

--
-- Constraints for table `credit_transaction`
--
ALTER TABLE `credit_transaction`
ADD CONSTRAINT `credit_transaction_ibfk_1` FOREIGN KEY (`account_number`) REFERENCES `account` (`account_number`);

--
-- Constraints for table `debit_transaction`
--
ALTER TABLE `debit_transaction`
ADD CONSTRAINT `debit_transaction_ibfk_1` FOREIGN KEY (`ledger_id`) REFERENCES `sale_ledger` (`ledger_id`);

--
-- Constraints for table `debit_transaction_record`
--
ALTER TABLE `debit_transaction_record`
ADD CONSTRAINT `debit_transaction_record_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `debit_transaction` (`transaction_id`),
ADD CONSTRAINT `debit_transaction_record_ibfk_2` FOREIGN KEY (`account_number`) REFERENCES `account` (`account_number`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
