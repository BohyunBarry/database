/* Create datbase */
DROP DATABASE IF EXISTS bank_db;
CREATE DATABASE bank_db;
USE bank_db;

/* create the table */
DROP TABLE IF EXISTS customer,account,credit_transaction,debit_transaction,debit_transaction_record,block_transaction,sale_ledger,transaction_record_audit;
CREATE TABLE customer ( 
	customer_id varchar(10) NOT NULL,
	customer_name varchar(20) NOT NULL,
	date_of_birth date, 
	gender char(1),
	phone_number varchar (12),
	address varchar(25), 
	PRIMARY KEY (customer_id)
	);

CREATE TABLE account (
	account_number	char(10) NOT NULL,
	customer_id varchar(10) NOT NULL,
	account_type varchar(10),
	balance decimal(10.2),
	PRIMARY KEY (account_number),
	FOREIGN KEY (customer_id) REFERENCES customer(customer_id)
	);

CREATE TABLE sale_ledger(
	ledger_id int NOT NULL,
	ledger_name varchar(20),
	PRIMARY KEY (ledger_id)
);
	
CREATE TABLE credit_transaction(
	transaction_id int NOT NULL AUTO_INCREMENT,
	account_number char(10),
	ordering_customer varchar(20),
	reference varchar(20),
	ordering_date date,
	amount 	decimal(10,2),
	PRIMARY KEY (transaction_id),
	FOREIGN KEY (account_number) REFERENCES account(account_number)
	);

CREATE TABLE debit_transaction(
	transaction_id char(6) NOT NULL,
	ledger_id int NOT NULL,
	transaction_category varchar(20),
	PRIMARY KEY (transaction_id),
	FOREIGN KEY (ledger_id) REFERENCES sale_ledger(ledger_id)
	);

CREATE TABLE debit_transaction_record (
	transaction_id char(6) NOT NULL,
	account_number char(10) NOT NULL,
	transaction_date date,
	description varchar(20),
	amount decimal(10,2),
	PRIMARY KEY (transaction_id,account_number),
	FOREIGN KEY (transaction_id) REFERENCES debit_transaction(transaction_id),
	FOREIGN KEY (account_number) REFERENCES account(account_number)
	);

CREATE TABLE block_transaction(
	transaction_id char(6) NOT NULL,
	account_number char(10) NOT NULL,
	PRIMARY KEY (transaction_id,account_number),
	FOREIGN KEY (transaction_id) REFERENCES debit_transaction(transaction_id),
	FOREIGN KEY (account_number) REFERENCES account(account_number)
);

CREATE TABLE transaction_log(
	message_id int(4) NOT NULL AUTO_INCREMENT,
	message varchar (255) NOT NULL,
	message_time timestamp,
	PRIMARY KEY (message_id)	
);

/* change the setting so that the account number can not be repeated */

ALTER TABLE `account` ADD UNIQUE(`account_number`);

/* grant PRIVILEGES to other user */
GRANT ALL PRIVILEGES
ON bank_db.*
TO bank_db_dev@localhost
IDENTIFIED BY '00000000';
